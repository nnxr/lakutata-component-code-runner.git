import {Exception} from '@lakutata/core'

export class RuntimeCodeException extends Exception {
    district: string
    errno: number | string = 'E_RUNTIME_CODE'
}
