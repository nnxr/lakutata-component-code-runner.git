import {Exception} from '@lakutata/core'

export class RuntimeCodeEnvironmentsException extends Exception {
    district: string
    errno: number | string = 'E_RUNTIME_CODE_ENVIRONMENTS'
}
