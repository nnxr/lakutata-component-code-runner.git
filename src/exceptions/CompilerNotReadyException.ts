import {Exception} from '@lakutata/core'

export class CompilerNotReadyException extends Exception {
    district: string
    errno: number | string = 'E_COMPILER_NOT_READY'
}
