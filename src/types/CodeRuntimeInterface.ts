import {Application} from '@lakutata/core'

export type CodeRuntimeInterface = (runCodeId: string, app: Application) => { [key: string]: (...args: any[]) => Promise<any> }
