export type RunCodeResultLog = {
    level: 'log' | 'info' | 'error' | 'warn'
    args: any[]
    timestamp: number
}
