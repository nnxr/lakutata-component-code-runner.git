import {Exception} from '@lakutata/core'
import {RunCodeResultLog} from './RunCodeResultLog'

export type RunCodeResult = {
    result: any
    exception: {
        err: string
        errMsg: string
        errno: number | string
    } | null
    logs: RunCodeResultLog[]
}
