import Ajv, {Schema, ValidateFunction} from 'ajv'

/**
 * 输出数据验证装饰器
 * @param jsonSchema
 * @constructor
 */
export const CodeOutputValidationDecorator = (jsonSchema: Schema) => {
    return (target: Object, propertyKey: string, descriptor: TypedPropertyDescriptor<(...args: any[]) => any | Promise<any>>) => {
        descriptor.writable = false
        const validator: ValidateFunction = new Ajv({allErrors: false}).compile(jsonSchema)
        const originalMethod: (...args: any[]) => any | Promise<any> = descriptor.value as any
        const validate = (originalMethodResult) => {
            if (validator(originalMethodResult)) {
                return originalMethodResult
            } else {
                const errors = validator.errors
                if (errors && errors[0]) {
                    const validateErrorObject = errors[0]
                    let errorMessage: string = validateErrorObject.message ? validateErrorObject.message : 'validate failed'
                    if (validateErrorObject.instancePath) {
                        let instancePath: string = validateErrorObject.instancePath.trim()
                        instancePath = instancePath.replaceAll(/\//g, '.')
                        if (instancePath.indexOf('.') === 0) {
                            instancePath = instancePath.substring(1)
                        }
                        if (instancePath) {
                            errorMessage = `${instancePath} ${errorMessage}`
                        }
                    }
                    throw new Error(errorMessage)
                } else {
                    throw new Error('Validate failed')
                }
            }
        }
        descriptor.value = function (...args: any[]) {
            const originalMethodResult = originalMethod.apply(this, args)
            if (originalMethodResult instanceof Promise) {
                return new Promise((resolve, reject) => {
                    return originalMethodResult.then(originalAsyncMethodResult => {
                        try {
                            resolve(validate(originalAsyncMethodResult))
                        } catch (e) {
                            reject(e)
                        }
                    }).catch(reject)
                })
            } else {
                return validate(originalMethodResult)
            }
        }
        return descriptor
    }
}
