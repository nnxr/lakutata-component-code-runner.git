import {EventEmitter} from 'events'
import {EventEmitter2} from 'eventemitter2'

export class BaseCodeClass {
    /**
     * 环境变量
     * @protected
     */
    protected readonly ENV: { [key: string]: any }

    /**
     * 数据交互接口
     * @protected
     */
    protected readonly CRI: { [key: string]: (...args: any[]) => Promise<any> }

    constructor() {
        const REE: EventEmitter = process['_$REE']
        const CRIEE: EventEmitter2 = process['_$CRIEE']
        const TIMEOUT: number = process['_$TIMEOUT']
        this.ENV = process['_$ENV']
        this.CRI = new Proxy({}, {
            get(target: {}, propertyKey: string): any {
                return new Proxy((...args: any[]) => {
                }, {
                    apply(target: (...args: any[]) => void, thisArg: any, argArray: any[]): any {
                        return new Promise((resolve, reject) => {
                            const argumentObject = {args: argArray}
                            CRIEE.emitAsync(propertyKey, JSON.stringify(argumentObject)).then(results => {
                                resolve(results[0])
                            }).catch(reject)
                        })
                    }
                })
            },
            set(target: {}, p: string | symbol, value: any, receiver: any): boolean {
                return true
            }
        })
        let finalized: boolean = false
        const resolve = (result: any) => {
            if (!finalized) {
                finalized = true
                REE.emit('result', result)
            }
        }
        const reject = (error: Error) => {
            if (!finalized) {
                finalized = true
                REE.emit('error', error)
            }
        }
        setTimeout(() => {
            return reject(new Error('Timeout'))
        }, TIMEOUT)
        this.main(this.CRI, this.ENV).then(result => {
            return resolve(result)
        }).catch(error => {
            return reject(error)
        })
    }

    public async main(CRI: { [key: string]: (...args: any[]) => Promise<any> }, ENV: { [key: string]: any }): Promise<any> {
        throw new Error('Empty Code')
    }
}
